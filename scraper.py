# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
from urllib2 import urlopen
import re
import csv

def main():
    base = 'http://www.eventsinamerica.com'
    t_s = '/trade-shows/'

    categories = get_categories_and_links(base + t_s)

    write_to_csv(categories, base)

    print 'Done..'

def extract_event_info(url):
    # Event Name | Event Attendance | Event Date | Website | Show Owner | Prim. Contact Name | Email | Phone
    soup = get_soup(url)
    try:
        event_name = ''.join(soup.find('div', class_='profile-content-right-a').find_all('h1')[1].get_text().strip())
    except Exception, e:
        print 'An exception occurred: ', e
        event_name = '-'
    
    try:
        event_attendance = ''.join(soup.find('span', class_='pis2').get_text().split())
    except Exception, e:
        print 'An exception occurred: ', e
        event_attendance = '-'
    
    try:
        start_date = ''.join(soup.find('div', class_='p-calendar org').find_all('span')[0].get_text())
    except Exception, e:
        print 'An exception occurred: ', e
        start_date = '-'

    try:
        end_date = ''.join(soup.find('div', class_='p-calendar org').find_all('span')[-1].get_text())
    except Exception, e:
        print 'An exception occurred: ', e
        end_date = '-'

    try:
        cal_month = soup.find('div', class_="p-title2").get_text().split(" ")
    except Exception, e:
        print 'An exception occurred: ', e
        cal_month = '-'
    
    try:
        event_start = '{} {}, {}'.format(cal_month[0], start_date, cal_month[-1])
    except Exception, e:
        print 'An exception occurred: ', e
        event_start = '-'

    try:
        event_end = '{} {}, {}'.format(cal_month[0], end_date, cal_month[-1])
    except Exception, e:
        print 'An exception occurred: ', e
        event_end = '-'
    
    try:
        event_website = ''.join(soup.find('a', class_='external-link').get('href'))
    except Exception, e:
        print 'An exception occurred: ', e
        event_website = '-'
    
    try:
        event_show_owner = ''.join(soup.find('div', class_='p-eventdet2').find('span').get_text())
    except Exception, e:
        print 'An exception occurred: ', e
        event_show_owner = '-'
    
    try:
        first_name = ''.join(soup.find('div', id='event-contacts').find('div', class_='alignleft').get_text().split(None, 4)[2])
    except Exception, e:
        print 'An exception occurred: ', e
        first_name = '-'

    try:
        last_name = ''.join(soup.find('div', id='event-contacts').find('div', class_='alignleft').get_text().split(None, 4)[3])
    except Exception, e:
        print 'An exception occurred: ', e
        last_name = '-'
    
    try:
        event_primary_contact_name = '{} {}'.format(first_name, last_name)
    except Exception, e:
        print 'An exception occurred: ', e
        event_primary_contact_name = '-'

    try:
        event_primary_contact_phone = ''.join(soup.find('a', class_="phone-link").get_text())
    except Exception, e:
        print 'An exception occurred: ', e
        event_primary_contact_phone = '-'
    
    return [event_name, event_attendance, event_start, event_end, event_website, event_show_owner, event_primary_contact_name, event_primary_contact_phone]

def get_categories_and_links(url):
    soup = get_soup(url)
    categories = soup.find_all('div', class_='sub-section')
    show_all = 'date/100000/1/'

    trade_shows = []

    for c in categories:
        category = c.find('h3').get_text()
        tradeshows = c.find_all('li')
        
        info = dict(category=category, tradeshows=[])
        for ts in tradeshows:
            index = tradeshows.index(ts)
            if ts.a.get('href') != '#':
                ts_name = ''.join(ts.a.get_text())
                if index == 0:
                    info['tradeshows'].append( { ts_name: ts.a.get('href') + 'general/' + show_all } )
                else:
                    info['tradeshows'].append( { ts_name: ts.a.get('href') + show_all } )

        trade_shows.append(info)

    return trade_shows

def get_soup(url):
    openurl = urlopen(url)
    return BeautifulSoup(openurl, 'html5lib')

def write_to_csv(categories, base):
    for o in categories:
        name_of_csv = o['category'].lower()
        with open('eoa_{}_trade_shows_and_events.csv'.format(name_of_csv), 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['Events In America Tradeshows and Events'])
            writer.writerow([])

            writer.writerow(['INDUSTRY'])
            writer.writerow([o['category']])
            writer.writerow([])
            
            for ts in o['tradeshows']:
                for name, link in ts.iteritems():
                    writer.writerow(['TRADE SHOW/EVENT/CONFERENCE/MEETING/CONVENTION'])
                    writer.writerow([name])

                    soup = get_soup(base + link)
                    rows = soup.find('table', id='present-events').find_all('tr')

                    writer.writerow(['EVENTS'])
                    writer.writerow(['Name', 'Attendance', 'Start Date', 'End Date', 'Website', 'Show Owner', 'Primary Contact Name', 'Primary Contact Phone #'])
                    print 'Rows: ', len(rows)
                    for r in rows:
                        try:
                            event_url = r.find('td', class_='c2').a.get('href')
                            event_info = extract_event_info(base + event_url)
                            print 'Writing event: {}'.format(event_info[0])
                            writer.writerow(event_info)
                        except Exception, e:
                            print 'Error occurred during events page: ', e
                            continue
                    writer.writerow([])

if __name__ == "__main__":
    main()
